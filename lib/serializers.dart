library serializers;

import 'package:built_value/serializer.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/standard_json_plugin.dart';

import 'package:openapi/model/mc_blog_post.dart';
import 'package:openapi/model/mc_event.dart';
import 'package:openapi/model/mc_place.dart';


part 'serializers.g.dart';

@SerializersFor(const [
MCBlogPost,
MCEvent,
MCPlace,

])

//allow all models to be serialized within a list
Serializers serializers = (_$serializers.toBuilder()
..addBuilderFactory(
const FullType(BuiltList, const [const FullType(MCBlogPost)]),
() => new ListBuilder<MCBlogPost>())
..addBuilderFactory(
const FullType(BuiltList, const [const FullType(MCEvent)]),
() => new ListBuilder<MCEvent>())
..addBuilderFactory(
const FullType(BuiltList, const [const FullType(MCPlace)]),
() => new ListBuilder<MCPlace>())

).build();

Serializers standardSerializers =
(serializers.toBuilder()..addPlugin(StandardJsonPlugin())).build();