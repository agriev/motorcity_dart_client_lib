import 'dart:async';
import 'dart:io';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';

import 'package:openapi/model/mc_blog_post.dart';

class PostsApi {
    final Dio _dio;
    Serializers _serializers;

    PostsApi(this._dio, this._serializers);

        /// 
        ///
        /// 
        Future<Response<MCBlogPost>>postsCreate(MCBlogPost data,{ CancelToken cancelToken, Map<String, String> headers,}) async {

            String path = "/posts/";

            // query params
            Map<String, dynamic> queryParams = {};
            Map<String, String> headerParams = Map.from(headers ?? {});
            Map<String, String> formParams = {};

            queryParams.removeWhere((key, value) => value == null);
            headerParams.removeWhere((key, value) => value == null);
            formParams.removeWhere((key, value) => value == null);

            List<String> contentTypes = [
                "application/json"];

            var serializedBody = _serializers.serialize(data);
            var jsondata = json.encode(serializedBody);

            return _dio.request(
            path,
            queryParameters: queryParams,
                data: jsondata,
            options: Options(
            method: 'post'.toUpperCase(),
            headers: headerParams,
            contentType: contentTypes.isNotEmpty ? contentTypes[0] : "application/json",
            ),
            cancelToken: cancelToken,
            ).then((response) {

        var serializer = _serializers.serializerForType(MCBlogPost);
        var data = _serializers.deserializeWith<MCBlogPost>(serializer, response.data);

            return Response<MCBlogPost>(
                data: data,
                headers: response.headers,
                request: response.request,
                redirects: response.redirects,
                statusCode: response.statusCode,
                statusMessage: response.statusMessage,
                extra: response.extra,
            );
            });
            }
        /// 
        ///
        /// 
        Future<Response>postsDelete(int id,{ CancelToken cancelToken, Map<String, String> headers,}) async {

            String path = "/posts/{id}/".replaceAll("{" + "id" + "}", id.toString());

            // query params
            Map<String, dynamic> queryParams = {};
            Map<String, String> headerParams = Map.from(headers ?? {});
            Map<String, String> formParams = {};

            queryParams.removeWhere((key, value) => value == null);
            headerParams.removeWhere((key, value) => value == null);
            formParams.removeWhere((key, value) => value == null);

            List<String> contentTypes = [];


            return _dio.request(
            path,
            queryParameters: queryParams,
            options: Options(
            method: 'delete'.toUpperCase(),
            headers: headerParams,
            contentType: contentTypes.isNotEmpty ? contentTypes[0] : "application/json",
            ),
            cancelToken: cancelToken,
            );
            }
        /// 
        ///
        /// 
        Future<Response<List<MCBlogPost>>>postsList({ CancelToken cancelToken, Map<String, String> headers,}) async {

            String path = "/posts/";

            // query params
            Map<String, dynamic> queryParams = {};
            Map<String, String> headerParams = Map.from(headers ?? {});
            Map<String, String> formParams = {};

            queryParams.removeWhere((key, value) => value == null);
            headerParams.removeWhere((key, value) => value == null);
            formParams.removeWhere((key, value) => value == null);

            List<String> contentTypes = [];


            return _dio.request(
            path,
            queryParameters: queryParams,
            options: Options(
            method: 'get'.toUpperCase(),
            headers: headerParams,
            contentType: contentTypes.isNotEmpty ? contentTypes[0] : "application/json",
            ),
            cancelToken: cancelToken,
            ).then((response) {

                final FullType type = const FullType(BuiltList, const [const FullType(MCBlogPost)]);
                BuiltList<MCBlogPost> dataList = _serializers.deserialize(response.data, specifiedType: type);
                var data = dataList.toList();

            return Response<List<MCBlogPost>>(
                data: data,
                headers: response.headers,
                request: response.request,
                redirects: response.redirects,
                statusCode: response.statusCode,
                statusMessage: response.statusMessage,
                extra: response.extra,
            );
            });
            }
        /// 
        ///
        /// 
        Future<Response<MCBlogPost>>postsPartialUpdate(int id,MCBlogPost data,{ CancelToken cancelToken, Map<String, String> headers,}) async {

            String path = "/posts/{id}/".replaceAll("{" + "id" + "}", id.toString());

            // query params
            Map<String, dynamic> queryParams = {};
            Map<String, String> headerParams = Map.from(headers ?? {});
            Map<String, String> formParams = {};

            queryParams.removeWhere((key, value) => value == null);
            headerParams.removeWhere((key, value) => value == null);
            formParams.removeWhere((key, value) => value == null);

            List<String> contentTypes = [
                "application/json"];

            var serializedBody = _serializers.serialize(data);
            var jsondata = json.encode(serializedBody);

            return _dio.request(
            path,
            queryParameters: queryParams,
                data: jsondata,
            options: Options(
            method: 'patch'.toUpperCase(),
            headers: headerParams,
            contentType: contentTypes.isNotEmpty ? contentTypes[0] : "application/json",
            ),
            cancelToken: cancelToken,
            ).then((response) {

        var serializer = _serializers.serializerForType(MCBlogPost);
        var data = _serializers.deserializeWith<MCBlogPost>(serializer, response.data);

            return Response<MCBlogPost>(
                data: data,
                headers: response.headers,
                request: response.request,
                redirects: response.redirects,
                statusCode: response.statusCode,
                statusMessage: response.statusMessage,
                extra: response.extra,
            );
            });
            }
        /// 
        ///
        /// 
        Future<Response<MCBlogPost>>postsRead(int id,{ CancelToken cancelToken, Map<String, String> headers,}) async {

            String path = "/posts/{id}/".replaceAll("{" + "id" + "}", id.toString());

            // query params
            Map<String, dynamic> queryParams = {};
            Map<String, String> headerParams = Map.from(headers ?? {});
            Map<String, String> formParams = {};

            queryParams.removeWhere((key, value) => value == null);
            headerParams.removeWhere((key, value) => value == null);
            formParams.removeWhere((key, value) => value == null);

            List<String> contentTypes = [];


            return _dio.request(
            path,
            queryParameters: queryParams,
            options: Options(
            method: 'get'.toUpperCase(),
            headers: headerParams,
            contentType: contentTypes.isNotEmpty ? contentTypes[0] : "application/json",
            ),
            cancelToken: cancelToken,
            ).then((response) {

        var serializer = _serializers.serializerForType(MCBlogPost);
        var data = _serializers.deserializeWith<MCBlogPost>(serializer, response.data);

            return Response<MCBlogPost>(
                data: data,
                headers: response.headers,
                request: response.request,
                redirects: response.redirects,
                statusCode: response.statusCode,
                statusMessage: response.statusMessage,
                extra: response.extra,
            );
            });
            }
        /// 
        ///
        /// 
        Future<Response<MCBlogPost>>postsUpdate(int id,MCBlogPost data,{ CancelToken cancelToken, Map<String, String> headers,}) async {

            String path = "/posts/{id}/".replaceAll("{" + "id" + "}", id.toString());

            // query params
            Map<String, dynamic> queryParams = {};
            Map<String, String> headerParams = Map.from(headers ?? {});
            Map<String, String> formParams = {};

            queryParams.removeWhere((key, value) => value == null);
            headerParams.removeWhere((key, value) => value == null);
            formParams.removeWhere((key, value) => value == null);

            List<String> contentTypes = [
                "application/json"];

            var serializedBody = _serializers.serialize(data);
            var jsondata = json.encode(serializedBody);

            return _dio.request(
            path,
            queryParameters: queryParams,
                data: jsondata,
            options: Options(
            method: 'put'.toUpperCase(),
            headers: headerParams,
            contentType: contentTypes.isNotEmpty ? contentTypes[0] : "application/json",
            ),
            cancelToken: cancelToken,
            ).then((response) {

        var serializer = _serializers.serializerForType(MCBlogPost);
        var data = _serializers.deserializeWith<MCBlogPost>(serializer, response.data);

            return Response<MCBlogPost>(
                data: data,
                headers: response.headers,
                request: response.request,
                redirects: response.redirects,
                statusCode: response.statusCode,
                statusMessage: response.statusMessage,
                extra: response.extra,
            );
            });
            }
        }
