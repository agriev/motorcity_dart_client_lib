        import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'mc_blog_post.g.dart';

abstract class MCBlogPost implements Built<MCBlogPost, MCBlogPostBuilder> {

    
        @nullable

    
    @BuiltValueField(wireName: 'id')
    int get id;
    
        @nullable

    
    @BuiltValueField(wireName: 'title')
    String get title;
    
        @nullable

    
    @BuiltValueField(wireName: 'body')
    String get body;
    
        @nullable

    
    @BuiltValueField(wireName: 'created_at')
    DateTime get createdAt;
    
        @nullable

    
    @BuiltValueField(wireName: 'modifiet_at')
    DateTime get modifietAt;

    // Boilerplate code needed to wire-up generated code
    MCBlogPost._();

    factory MCBlogPost([updates(MCBlogPostBuilder b)]) = _$MCBlogPost;
    static Serializer<MCBlogPost> get serializer => _$mCBlogPostSerializer;

}

