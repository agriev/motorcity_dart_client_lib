        import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'mc_place.g.dart';

abstract class MCPlace implements Built<MCPlace, MCPlaceBuilder> {

    
        @nullable

    
    @BuiltValueField(wireName: 'id')
    int get id;
    
        @nullable

    
    @BuiltValueField(wireName: 'name')
    String get name;
    
        @nullable

    
    @BuiltValueField(wireName: 'logo_image')
    String get logoImage;
    
        @nullable

    
    @BuiltValueField(wireName: 'short_description')
    String get shortDescription;
    
        @nullable

    
    @BuiltValueField(wireName: 'city')
    String get city;
    
        @nullable

    
    @BuiltValueField(wireName: 'address')
    String get address;
    
        @nullable

    
    @BuiltValueField(wireName: 'location')
    String get location;

    // Boilerplate code needed to wire-up generated code
    MCPlace._();

    factory MCPlace([updates(MCPlaceBuilder b)]) = _$MCPlace;
    static Serializer<MCPlace> get serializer => _$mCPlaceSerializer;

}

