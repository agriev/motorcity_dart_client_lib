        import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'mc_event.g.dart';

abstract class MCEvent implements Built<MCEvent, MCEventBuilder> {

    
        @nullable

    
    @BuiltValueField(wireName: 'id')
    int get id;
    
        @nullable

    
    @BuiltValueField(wireName: 'title')
    String get title;
    
        @nullable

    
    @BuiltValueField(wireName: 'body')
    String get body;
    
        @nullable

    
    @BuiltValueField(wireName: 'created_at')
    DateTime get createdAt;
    
        @nullable

    
    @BuiltValueField(wireName: 'modifiet_at')
    DateTime get modifietAt;
    
        @nullable

    
    @BuiltValueField(wireName: 'short_description')
    String get shortDescription;
    
        @nullable

    
    @BuiltValueField(wireName: 'logo_image')
    String get logoImage;
    
        @nullable

    
    @BuiltValueField(wireName: 'date_start')
    DateTime get dateStart;
    
        @nullable

    
    @BuiltValueField(wireName: 'date_end')
    DateTime get dateEnd;
    
        @nullable

    
    @BuiltValueField(wireName: 'location')
    int get location;

    // Boilerplate code needed to wire-up generated code
    MCEvent._();

    factory MCEvent([updates(MCEventBuilder b)]) = _$MCEvent;
    static Serializer<MCEvent> get serializer => _$mCEventSerializer;

}

