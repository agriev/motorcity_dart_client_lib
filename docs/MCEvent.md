# openapi.model.MCEvent

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] [default to null]
**title** | **String** |  | [default to null]
**body** | **String** |  | [default to null]
**createdAt** | [**DateTime**](DateTime.md) |  | [optional] [default to null]
**modifietAt** | [**DateTime**](DateTime.md) |  | [optional] [default to null]
**shortDescription** | **String** |  | [optional] [default to null]
**logoImage** | **String** |  | [optional] [default to null]
**dateStart** | [**DateTime**](DateTime.md) |  | [optional] [default to null]
**dateEnd** | [**DateTime**](DateTime.md) |  | [optional] [default to null]
**location** | **int** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


