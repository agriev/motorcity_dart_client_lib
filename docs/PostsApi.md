# openapi.api.PostsApi

## Load the API package
```dart
import 'package:openapi/api.dart';
```

All URIs are relative to *http://0.0.0.0:8000/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**postsCreate**](PostsApi.md#postsCreate) | **post** /posts/ | 
[**postsDelete**](PostsApi.md#postsDelete) | **delete** /posts/{id}/ | 
[**postsList**](PostsApi.md#postsList) | **get** /posts/ | 
[**postsPartialUpdate**](PostsApi.md#postsPartialUpdate) | **patch** /posts/{id}/ | 
[**postsRead**](PostsApi.md#postsRead) | **get** /posts/{id}/ | 
[**postsUpdate**](PostsApi.md#postsUpdate) | **put** /posts/{id}/ | 


# **postsCreate**
> MCBlogPost postsCreate(data)



### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Basic
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').password = 'YOUR_PASSWORD';

var api_instance = new PostsApi();
var data = new MCBlogPost(); // MCBlogPost | 

try { 
    var result = api_instance.postsCreate(data);
    print(result);
} catch (e) {
    print("Exception when calling PostsApi->postsCreate: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**MCBlogPost**](MCBlogPost.md)|  | 

### Return type

[**MCBlogPost**](MCBlogPost.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **postsDelete**
> postsDelete(id)



### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Basic
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').password = 'YOUR_PASSWORD';

var api_instance = new PostsApi();
var id = 56; // int | A unique integer value identifying this mc blog post.

try { 
    api_instance.postsDelete(id);
} catch (e) {
    print("Exception when calling PostsApi->postsDelete: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this mc blog post. | [default to null]

### Return type

void (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **postsList**
> List<MCBlogPost> postsList()



### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Basic
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').password = 'YOUR_PASSWORD';

var api_instance = new PostsApi();

try { 
    var result = api_instance.postsList();
    print(result);
} catch (e) {
    print("Exception when calling PostsApi->postsList: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List<MCBlogPost>**](MCBlogPost.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **postsPartialUpdate**
> MCBlogPost postsPartialUpdate(id, data)



### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Basic
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').password = 'YOUR_PASSWORD';

var api_instance = new PostsApi();
var id = 56; // int | A unique integer value identifying this mc blog post.
var data = new MCBlogPost(); // MCBlogPost | 

try { 
    var result = api_instance.postsPartialUpdate(id, data);
    print(result);
} catch (e) {
    print("Exception when calling PostsApi->postsPartialUpdate: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this mc blog post. | [default to null]
 **data** | [**MCBlogPost**](MCBlogPost.md)|  | 

### Return type

[**MCBlogPost**](MCBlogPost.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **postsRead**
> MCBlogPost postsRead(id)



### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Basic
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').password = 'YOUR_PASSWORD';

var api_instance = new PostsApi();
var id = 56; // int | A unique integer value identifying this mc blog post.

try { 
    var result = api_instance.postsRead(id);
    print(result);
} catch (e) {
    print("Exception when calling PostsApi->postsRead: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this mc blog post. | [default to null]

### Return type

[**MCBlogPost**](MCBlogPost.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **postsUpdate**
> MCBlogPost postsUpdate(id, data)



### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Basic
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').password = 'YOUR_PASSWORD';

var api_instance = new PostsApi();
var id = 56; // int | A unique integer value identifying this mc blog post.
var data = new MCBlogPost(); // MCBlogPost | 

try { 
    var result = api_instance.postsUpdate(id, data);
    print(result);
} catch (e) {
    print("Exception when calling PostsApi->postsUpdate: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this mc blog post. | [default to null]
 **data** | [**MCBlogPost**](MCBlogPost.md)|  | 

### Return type

[**MCBlogPost**](MCBlogPost.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

