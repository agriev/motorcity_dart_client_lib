# openapi.model.MCPlace

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] [default to null]
**name** | **String** |  | [optional] [default to null]
**logoImage** | **String** |  | [optional] [default to null]
**shortDescription** | **String** |  | [optional] [default to null]
**city** | **String** |  | [optional] [default to null]
**address** | **String** |  | [optional] [default to null]
**location** | **String** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


