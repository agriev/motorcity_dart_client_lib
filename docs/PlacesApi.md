# openapi.api.PlacesApi

## Load the API package
```dart
import 'package:openapi/api.dart';
```

All URIs are relative to *http://0.0.0.0:8000/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**placesCreate**](PlacesApi.md#placesCreate) | **post** /places/ | 
[**placesDelete**](PlacesApi.md#placesDelete) | **delete** /places/{id}/ | 
[**placesList**](PlacesApi.md#placesList) | **get** /places/ | 
[**placesPartialUpdate**](PlacesApi.md#placesPartialUpdate) | **patch** /places/{id}/ | 
[**placesRead**](PlacesApi.md#placesRead) | **get** /places/{id}/ | 
[**placesUpdate**](PlacesApi.md#placesUpdate) | **put** /places/{id}/ | 


# **placesCreate**
> MCPlace placesCreate(data)



### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Basic
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').password = 'YOUR_PASSWORD';

var api_instance = new PlacesApi();
var data = new MCPlace(); // MCPlace | 

try { 
    var result = api_instance.placesCreate(data);
    print(result);
} catch (e) {
    print("Exception when calling PlacesApi->placesCreate: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**MCPlace**](MCPlace.md)|  | 

### Return type

[**MCPlace**](MCPlace.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **placesDelete**
> placesDelete(id)



### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Basic
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').password = 'YOUR_PASSWORD';

var api_instance = new PlacesApi();
var id = 56; // int | A unique integer value identifying this mc place.

try { 
    api_instance.placesDelete(id);
} catch (e) {
    print("Exception when calling PlacesApi->placesDelete: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this mc place. | [default to null]

### Return type

void (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **placesList**
> List<MCPlace> placesList()



### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Basic
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').password = 'YOUR_PASSWORD';

var api_instance = new PlacesApi();

try { 
    var result = api_instance.placesList();
    print(result);
} catch (e) {
    print("Exception when calling PlacesApi->placesList: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List<MCPlace>**](MCPlace.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **placesPartialUpdate**
> MCPlace placesPartialUpdate(id, data)



### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Basic
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').password = 'YOUR_PASSWORD';

var api_instance = new PlacesApi();
var id = 56; // int | A unique integer value identifying this mc place.
var data = new MCPlace(); // MCPlace | 

try { 
    var result = api_instance.placesPartialUpdate(id, data);
    print(result);
} catch (e) {
    print("Exception when calling PlacesApi->placesPartialUpdate: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this mc place. | [default to null]
 **data** | [**MCPlace**](MCPlace.md)|  | 

### Return type

[**MCPlace**](MCPlace.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **placesRead**
> MCPlace placesRead(id)



### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Basic
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').password = 'YOUR_PASSWORD';

var api_instance = new PlacesApi();
var id = 56; // int | A unique integer value identifying this mc place.

try { 
    var result = api_instance.placesRead(id);
    print(result);
} catch (e) {
    print("Exception when calling PlacesApi->placesRead: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this mc place. | [default to null]

### Return type

[**MCPlace**](MCPlace.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **placesUpdate**
> MCPlace placesUpdate(id, data)



### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Basic
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').password = 'YOUR_PASSWORD';

var api_instance = new PlacesApi();
var id = 56; // int | A unique integer value identifying this mc place.
var data = new MCPlace(); // MCPlace | 

try { 
    var result = api_instance.placesUpdate(id, data);
    print(result);
} catch (e) {
    print("Exception when calling PlacesApi->placesUpdate: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this mc place. | [default to null]
 **data** | [**MCPlace**](MCPlace.md)|  | 

### Return type

[**MCPlace**](MCPlace.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

