# openapi.api.EventsApi

## Load the API package
```dart
import 'package:openapi/api.dart';
```

All URIs are relative to *http://0.0.0.0:8000/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**eventsCreate**](EventsApi.md#eventsCreate) | **post** /events/ | 
[**eventsDelete**](EventsApi.md#eventsDelete) | **delete** /events/{id}/ | 
[**eventsList**](EventsApi.md#eventsList) | **get** /events/ | 
[**eventsPartialUpdate**](EventsApi.md#eventsPartialUpdate) | **patch** /events/{id}/ | 
[**eventsRead**](EventsApi.md#eventsRead) | **get** /events/{id}/ | 
[**eventsUpdate**](EventsApi.md#eventsUpdate) | **put** /events/{id}/ | 


# **eventsCreate**
> MCEvent eventsCreate(data)



### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Basic
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').password = 'YOUR_PASSWORD';

var api_instance = new EventsApi();
var data = new MCEvent(); // MCEvent | 

try { 
    var result = api_instance.eventsCreate(data);
    print(result);
} catch (e) {
    print("Exception when calling EventsApi->eventsCreate: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**MCEvent**](MCEvent.md)|  | 

### Return type

[**MCEvent**](MCEvent.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **eventsDelete**
> eventsDelete(id)



### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Basic
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').password = 'YOUR_PASSWORD';

var api_instance = new EventsApi();
var id = 56; // int | A unique integer value identifying this mc event.

try { 
    api_instance.eventsDelete(id);
} catch (e) {
    print("Exception when calling EventsApi->eventsDelete: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this mc event. | [default to null]

### Return type

void (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **eventsList**
> List<MCEvent> eventsList()



### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Basic
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').password = 'YOUR_PASSWORD';

var api_instance = new EventsApi();

try { 
    var result = api_instance.eventsList();
    print(result);
} catch (e) {
    print("Exception when calling EventsApi->eventsList: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List<MCEvent>**](MCEvent.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **eventsPartialUpdate**
> MCEvent eventsPartialUpdate(id, data)



### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Basic
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').password = 'YOUR_PASSWORD';

var api_instance = new EventsApi();
var id = 56; // int | A unique integer value identifying this mc event.
var data = new MCEvent(); // MCEvent | 

try { 
    var result = api_instance.eventsPartialUpdate(id, data);
    print(result);
} catch (e) {
    print("Exception when calling EventsApi->eventsPartialUpdate: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this mc event. | [default to null]
 **data** | [**MCEvent**](MCEvent.md)|  | 

### Return type

[**MCEvent**](MCEvent.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **eventsRead**
> MCEvent eventsRead(id)



### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Basic
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').password = 'YOUR_PASSWORD';

var api_instance = new EventsApi();
var id = 56; // int | A unique integer value identifying this mc event.

try { 
    var result = api_instance.eventsRead(id);
    print(result);
} catch (e) {
    print("Exception when calling EventsApi->eventsRead: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this mc event. | [default to null]

### Return type

[**MCEvent**](MCEvent.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **eventsUpdate**
> MCEvent eventsUpdate(id, data)



### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Basic
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Basic').password = 'YOUR_PASSWORD';

var api_instance = new EventsApi();
var id = 56; // int | A unique integer value identifying this mc event.
var data = new MCEvent(); // MCEvent | 

try { 
    var result = api_instance.eventsUpdate(id, data);
    print(result);
} catch (e) {
    print("Exception when calling EventsApi->eventsUpdate: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this mc event. | [default to null]
 **data** | [**MCEvent**](MCEvent.md)|  | 

### Return type

[**MCEvent**](MCEvent.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

